(function (require, module) {

   module.exports = functions = {
      
      Parse: function Parse(warcFile, enc, done) {
         // console.error(arguments.callee.toString().match(/function (.*?)\(/)[1])
         warcFile = warcFile.toString("UTF-8")
         var date = warcFile.match(/WARC-Date: (.*)/)
         date = date ? date[1] : "N/A"

         var source = warcFile.match(/WARC-Target-URI: (.*)/)
         source = source ? source[1] : "N/A"
         
         var emailRegex = /([a-zA-Z0-9._-]+)@([a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi 
         var stream = this
         warcFile.replace(emailRegex, function (match, $1, $2) {
            var row = {}
            row.email = match
            row.source = source
            row.date = date
            stream.push(row)
         })
         done()
      },

      Split: function Split(chunk, encoding, done) {
         // console.error(arguments.callee.toString().match(/function (.*?)\(/)[1])
         var stream = this
         var parts = chunk.toString("UTF-8").split(/(.*?WARC\/1\.0)/g)
         if (parts.length) {
            for (var c = 0; c < parts.length; c++) {
               var part = parts[c]
               if (part) stream.push(part)
            }
         } else {
            stream.push(chunk)
         }
         done()
      },

      CsvWriteStream: function CsvWriteStream(rowObj, enc, done) {
         // console.error(arguments.callee.toString().match(/function (.*?)\(/)[1])
         var row = []
         for (var header in rowObj) {
            row.push((rowObj[header]).toString().replace(/"/g, ""))
         }
         var rowText = '"' + row.join('","') + '"\n'

         var stream = this
         stream.push(rowText)
         done()
      },

      toJson: function toJson(buf, enc, done) {
         var stream = this
         var text = buf.toString("UTF-8")
         // Validate JSON as sometimes we receive incomplete stuff in the stream
         if (!text.replace(/\n/g, "").match(/\{.*\}/)) { done(); return }

         text.replace(/\n/g, "").split(/(?=\{.*\})/).forEach(function (json, i) {
            // console.log("\n\n%s\n\n", json)
            stream.push(JSON.parse(json))
         })
         done()
      },

      toText: function toText(buf, enc, done) {
         var stream = this
         var text
         if (buf instanceof Buffer || typeof buf === 'string') {
            text = buf.toString("UTF-8")
         } else {
            text = JSON.stringify(buf)
         }
         stream.push(text)
         done()
      }
   }

})(require, module)
