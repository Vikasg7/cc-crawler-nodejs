(function (require, module, process) {

   var initCrawler = require("./cc-crawler.js")

   process.stdin
      .on("data", initCrawler)

})(require, module, process)