(function (require, module, stringify) {

   var spawn = require("child_process").spawn
   var fs = require("fs")
   var Streamify = require("./Streamify.js")
   var functions = require("./functions.js")
   var toJson = functions.toJson
   var toText = functions.toText
   var storedData = JSON.parse(fs.readFileSync("./localStorage.txt", "UTF-8")) 
   var path = require("path")
   var logger = require("./logger.js")

   function StartWorker(urls) {
      // console.error(arguments.callee.toString().match(/function (.*?)\(/)[1])
      var child = spawn("node", ["./js/worker.js"])

      child.stdout
        .pipe(Streamify(toJson))
        .on("data", logData)
        .on("data", sendWarcFile)
      
      // For handling errors
      child.stderr.on("data", function (data) {
         fs.appendFileSync("./logs.txt", data.toString(), "UTF-8")
      })

      child.on("exit", function () { logger.workerDone +=1; logger.logIt() })

      logger.totWorkers += 1; logger.logIt()

      // Started here
      sendNextUrl()

      function sendWarcFile(data) {
         // console.error(arguments.callee.toString().match(/function (.*?)\(/)[1])
         if (data.sendWarcFile) {
            storedData[data.fName].done = true
            fs.writeFileSync("./localStorage.txt", stringify(storedData), "UTF-8")
            sendNextUrl()
         }
      }

      function sendNextUrl() {
         for (var i = 0; i < urls.length; i++) {
            var url = urls[i]
            var fName = path.parse(url).name
            if (storedData[fName]) {
               if (!storedData[fName].done) {
                  child.stdin.write(url)
                  urls.splice(i, 1)
                  break;
               }
            } else {
               if (!storedData[fName]) { storedData[fName] = {} } 
               child.stdin.write(url)
               urls.splice(i, 1)
               break;
            }
         }
      }

      function logData(data) {
         // console.error(arguments.callee.toString().match(/function (.*?)\(/)[1])
         if (data.startFrom) {
            if (!storedData[data.fName]) { storedData[data.fName] = {} }
            storedData[data.fName].startFrom = data.startFrom
            fs.writeFileSync("./localStorage.txt", stringify(storedData, null, 3), "UTF-8")
         } else if (data.bytesProcessed) {
            logger.bytesProcessed += data.bytesProcessed
            logger.logIt()
         } else if (data.emailCount) {
            logger.emailCount += data.emailCount
            logger.logIt()
         }
      }
   }

   module.exports = StartWorker

})(require, module, JSON.stringify)