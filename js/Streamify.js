(function (require, module) {

   var Transform = require("stream").Transform
   var util = require("util")

   // Constructor function
   function Streamify(transformFn, flushFn) {
      Transform.call(this, {objectMode: true})
      this._transform = transformFn
      this._flush = flushFn || function (done) { done() }
   }
   
   util.inherits(Streamify, Transform)
   
   module.exports = function (transformFn, flushFn) {
      return new Streamify(transformFn, flushFn)
   }

})(require, module)