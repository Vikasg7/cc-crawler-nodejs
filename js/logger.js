(function (require, module) {

   var fs = require("fs")
   module.exports = logger = {
      emailCount: 0,
      filesDone: 0,
      bytesProcessed: 0,
      workersDone: 0,
      totWorkers: 0,

      logIt: function logIt() {
         process.stdout.moveCursor(0, -6)
         process.stdout.cursorTo(0)
         process.stdout.clearScreenDown()
         var stdMsg = [
              "Progress      : ", this.emailCount,
            "\nFiles Done    : ", this.filesDone,
            "\nData processed: ", this.roundUp(this.bytesProcessed / (1024 * 1024 * 1024), 3) + " GBs",
            "\nData / Sec    : ", this.roundUp((this.bytesProcessed / (1024 * 1024)) / process.uptime(), 3) + " MBs",
            "\nWorkers Done  : ", this.workersDone,
            "\nTotal Workers : ", this.totWorkers,
            "\nTime used     : ", this.getTime(process.uptime())
         ]
         var progress = stdMsg.join("")
         process.stdout.write(progress)
         fs.writeFileSync("./progress.txt", progress, "UTF-8")
      },

      getTime: function (seconds) {
         var date = new Date(null)
         date.setSeconds(seconds)
         return date.toUTCString().match(/[0-9]{2}\:[0-9]{2}\:[0-9]{2}/g)[0]
      },

      roundUp: function (num, decimals) { 
         return (Math.ceil(num * Math.pow(10, decimals)) / Math.pow(10, decimals))
      }
   }


})(require, module)