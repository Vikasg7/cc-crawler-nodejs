(function (require, module, stringify) {

   var fs = require("fs")
   var zlib = require("zlib")
   var functions = require("./functions.js")
   var Streamify = require("./Streamify.js")
   var path = require("path")
   var request = require("request")
   var format = require("util").format

   function initCrawler(url) {
      url = url.toString("UTF-8").trim()
      var fName = path.parse(url).name
      var storedData = JSON.parse(fs.readFileSync("./localStorage.txt", "UTF-8"))
      if (!storedData[fName]) { storedData[fName] = {} } 

      var options = {
         splitRegex: /(.*?WARC\/1\.0)/g,
         warcRegex: /WARC\/1\.0([\S\s]+?)WARC\/1\.0/,
         startFrom: storedData[fName].startFrom || 0
      }

      var Split = functions.Split
      var GetWarcFile = require("./GetWarcFile.js")(options)
      var Parse = functions.Parse
      var CsvWriteStream = functions.CsvWriteStream
      var suffix = new Date().toLocaleTimeString().replace(/\:/g, "_")
      var outputFile = format("./outputs/%s.csv", fName)

      // console.error(url)
      request.get(url)
        .on("error", function (error) { console.error(error.toString) })
        .on("data", bytesProcessed)
        .on("end", orderNextWarcFile)
        .pipe(zlib.createGunzip())
        .pipe(Streamify(Split))
        .pipe(GetWarcFile)
        .pipe(Streamify(Parse))
        .pipe(Streamify(CsvWriteStream))
        .on("data", logStuff)
        .pipe(fs.createWriteStream(outputFile, {flags: "a"}))

      var logs = {
         totBytesProcessed: 0,
         totEmailCount: 0  
      }

      function logStuff() {
         logs.totEmailCount += 1
         if (logs.totEmailCount > 20) {
            // Logging emailCount
            console.log(stringify({emailCount: logs.totEmailCount}))
            logs.totEmailCount = 0
            // Logging bytesProcessed
            console.log(stringify({bytesProcessed: logs.totBytesProcessed}))
            logs.totBytesProcessed = 0
            // Logging startFrom
            console.log(stringify({startFrom: GetWarcFile.startFrom, fName: fName}))
         }
      }

      function bytesProcessed(data) {
         logs.totBytesProcessed += data.length
      }

      function orderNextWarcFile() {
         console.log(stringify({sendWarcFile: true, fName: fName}))
      }
   }

   module.exports = initCrawler
   
})(require, module, JSON.stringify)