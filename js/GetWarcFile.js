(function (require, module) {

   var Transform = require("stream").Transform
   var util = require("util")

   // Extending content constructor with Transform stream
   function GetWarcFile(options) {
      this.startFrom = options.startFrom || 0
      this.warcRegex = options.warcRegex || /WARC\/1\.0([\S\s]+?)WARC\/1\.0/
      this.data = []
      this.contentLength = 0   
      Transform.call(this)
   }

   util.inherits(GetWarcFile, Transform)

   GetWarcFile.prototype.matchAndPush = function () {
      var joined = this.data.join("")
      var matched = joined.match(this.warcRegex)
      if (matched) {
         // Pushing content to the stream
         this.push(matched[1])
         // Keeping on the last element in the data array
         this.data = this.data.slice(-1)
         // Marking the startpoint for the next time
         this.startFrom = this.contentLength - this.data[0].length
      }
   }

   // GetWarcFile is a Tansform Stream now
   GetWarcFile.prototype._transform = function (chunk, encoding, done) {
      this.contentLength += chunk.length
      if (this.contentLength >= this.startFrom) {
         this.data.push(chunk)
         this.matchAndPush()
      }
      done()
   }

   GetWarcFile.prototype._flush = function (done) {
      this.data.push("WARC/1.0")
      this.matchAndPush()
      this.data = null
      done()
   }

   module.exports = function (options) {
      return new GetWarcFile(options)
   }

})(require, module)

