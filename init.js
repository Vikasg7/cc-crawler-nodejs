(function (require, module) {

    var args = require("yargs")
               .usage("Usage: node init [Options]")
               
               .demand("p")
               .alias("p", "processCount")
               .describe("p", "No. of workers to start.")
               .default("p", 1)
               
               .help("h")
               .alias("h", "help")
               .epilog("Copyright 2016 Vikas Gautam")
               .argv


   var StartWorker = require("./js/StartWorker.js")
   var fs = require("fs")
   var urls = fs.readFileSync("./paths/warc.paths", "UTF-8").trim().split(/\r\n|\n/g)

   console.log("\n\n")

   for (var c = 0; c < args.p; c++) {
      StartWorker(urls)
   }   
   
})(require, module)

